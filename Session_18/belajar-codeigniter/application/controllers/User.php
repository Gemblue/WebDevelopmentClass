<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    // Menampilkan halaman daftar user.
    public function index()
	{
        $this->load->model('user_model');

        $data['users'] = $this->user_model->getUsers();
        
        $this->load->view('user', $data);
    }

    // Menampilkan halaman login user.
	public function login()
	{
        $this->load->view('login');
    }
}