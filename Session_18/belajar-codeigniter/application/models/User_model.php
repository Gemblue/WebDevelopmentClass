<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    public function getUsers()
    {
        // Ceritanya ini dari database :)
        $users = [
            [
                'name' => 'Sofian',
                'umur' => 19,
                'address' => 'Depok'
            ],
            [
                'name' => 'Fahmi',
                'umur' => 20,
                'address' => 'Bandung'
            ],
            [
                'name' => 'Isman',
                'umur' => 19,
                'address' => 'Surabaya'
            ]
        ];

        return $users;
    }

}