<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Halaman Daftar User</title>
</head>
<body>

<div id="container">
	<h1>Halaman Datar User</h1>
    
    <table border="1">
        <tr>
            <th>name</th>
            <th>umur</th>
            <th>address</th>
        </tr>
        
        <?php foreach($users as $user) :?>
            <tr>
                <td><?php echo $user['name'];?></td>
                <td><?php echo $user['umur'];?></td>
                <td><?php echo $user['address'];?></td>
            </tr>
        <?php endforeach;?>

    </table>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. </p>
</div>

</body>
</html>