<?php
/**
 * Class itu adalah kumpulan data / variabel dan fungsi yang menyatu (encapsulate) untuk keperluan
 * business process tertentu
 */

// Latihan membuat class ..
class User {

    // Public, private, protected
    private $nama;
    public $email;
    public $password;

    function set($nama)
    {
        $this->nama = $nama;

        return $this->nama;
    }

    public function insert()
    {
        // Disini kode kode insert.
        return $this->__bold('Berhasil insert data!');  
    }

    public function update()
    {
        // Disini kode kode update.
        return 'Berhasil update data!'; 
    } 

    protected function delete()
    {
        // Disini kode kode delete.
        return 'Berhasil delete data!'; 
    }

    private function __bold($string)
    {
        $string = '<b>' . $string . '</b>';

        return $string;
    }
}

class Profile extends User
{
    public function delete_profile()
    {
        return $this->delete();
    }
}

// Disini kita pakai classnya..
$profile = new Profile();

echo $profile->delete_profile();
