<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Data Aktor Film</title>
<link  href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">

</head>
<body>

<div class="container">
    <div class="header">
        <h2>DAFTAR 10 KOMIKA YANG MENJADI AKTOR FILM</h2>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>                
                <th>Photo</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Tgl Lahir</th>
                <th>Link Profile</th>
                <th>Film</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $artists = [
            [
                "nama" => "Mongol stres",
                "umur" => "39 Tahun",
                "photo" => "images/mongol.jpg",
                "tgl_lahir" => "27 September 1978",
                "profile" => "https://www.instagram.com/mongolstres/",
                "film" => "The Secret : Suster Ngesot Urban Legend"
            ],
            [
                "nama" => "Arie Kriting",
                "umur" => "33 Tahun",
                "photo" => "images/arie.jpg",
                "tgl_lahir" => "13 April 1985",
                "profile" => "https://www.instagram.com/arie_kriting/?hl=id",
                "film" => "Jomblo Reboot"
            ],
            [
                "nama" => "Boris Bokir",
                "umur" => "29 Tahun",
                "photo" => "images/boris.jpg",
                "tgl_lahir" => "25 Mei 1988",
                "profile" => "https://www.instagram.com/borisbokir_/?hl=id",
                "film" => "Rudy Habibie"
            ],
            [
                "nama" => "Ernest Prakasa",
                "umur" => "36 Tahun",
                "photo" => "images/ernest.jpg",
                "tgl_lahir" => "29 Januari 1982",
                "profile" => "https://www.instagram.com/ernestprakasa/?hl=id",
                "film" => "Cek Toko Sebelah"
            ],
            [
                "nama" => "Dodit Mulyanto",
                "umur" => "32 Tahun",
                "photo" => "images/dodit.jpg",
                "tgl_lahir" => "30 Juni 1985",
                "profile" => "https://www.instagram.com/dodit_mul/",
                "film" => "Partikelir"
            ],
            [
                "nama" => "Ge Pamungkas",
                "umur" => "29 Tahun",
                "photo" => "images/ge.jpg",
                "tgl_lahir" => "25 Januari 1989",
                "profile" => "https://www.instagram.com/gepamungkas/?hl=id",
                "film" => "Mars Met Venus"
            ],
            [
                "nama" => "Pandji Pragiwaksono",
                "umur" => "38 Tahun",
                "photo" => "images/pandji.jpg",
                "tgl_lahir" => "18 Juni 1979",
                "profile" => "https://www.instagram.com/pandji.pragiwaksono/?hl=id",
                "film" => "Insya Allah Sah"
            ],
            [
                "nama" => "Babe Cabiita",
                "umur" => "28 Tahun",
                "photo" => "images/babe.jpg",
                "tgl_lahir" => "05 Juni 1989",
                "profile" => "https://www.instagram.com/babecabiita/?hl=id",
                "film" => "Takut Kawin"
            ],
            [
                "nama" => "Raditya Dika",
                "umur" => "33 Tahun",
                "photo" => "images/dika.jpg",
                "tgl_lahir" => "28 Desember 1984",
                "profile" => "https://www.instagram.com/raditya_dika/?hl=id",
                "film" => "Hangout"
            ],
            [
                "nama" => "Kemal Palevi",
                "umur" => "28 Tahun",
                "photo" => "images/kemal.jpg",
                "tgl_lahir" => "25 Agustus 1989",
                "profile" => "https://www.instagram.com/kemalpalevi/?hl=id",
                "film" => "Keluarga Tak Kasat Mata"
            ]
                
        ];
            
        foreach ($artists as $artist) 
        {
            ?>
            <tr>                    
                <td><img  width="100" src="<?php echo $artist['photo'] ?>"></td>
                <td><?php echo $artist['nama'] ?></td>
                <td><?php echo $artist['umur'] ?></td>
                <td><?php echo $artist['tgl_lahir'] ?></td>
                <td><a class="profil profillink" href="<?php echo $artist['profile'] ?>" target="blank">Profile</a></td>
                <td><?php echo $artist['film'] ?></td>
            </tr>
            <?php
        }
            
        ?>
            
            
        </tbody>
        </table>
    </div>
    </body>
    </html>