<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Bootstrap</title>    
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" >
    
</head>
<body>
    
    <?php
    include "navbar.php";
    ?>
    
    <section class="about">
        <div class="container" >
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>ABOUT</h2>
                    <hr>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-5 col-md-offset-1">
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci provident repudiandae blanditiis. Deleniti perferendis obcaecati eveniet? Harum sapiente, nobis eligendi saepe qui, tenetur molestias dolor nulla neque reiciendis quia quidem. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Veritatis natus accusantium velit voluptate incidunt excepturi fuga, quisquam, blanditiis sequi a voluptatem odit, quae quaerat soluta. Inventore a non perferendis eveniet?</p>
                </div>
                <div class="col-md-5">
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Optio sit fugit est repellat amet nihil, atque ad veritatis mollitia quasi quis facilis asperiores sed. Unde dicta ipsam sed? Sit, autem! Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odio omnis voluptas sapiente veritatis reiciendis officiis voluptates aliquid, eum nihil accusantium nobis ad odit nam ea minima impedit neque suscipit eos?</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-default">
                        <span class="glyphicon glyphicon-save"></span> Download Now!
                    </button>
                </div>
            </div>
        </div>
    </section>
    
    <section class="footer">
        <div class="container">
            <div class="row text-center ">
                <div class="col-md-12">
                    <p>Copyright &copy; My Bootstrap 2018</p>
                </div>
            </div>
        </div>
    </section>
    
</body>
</html>