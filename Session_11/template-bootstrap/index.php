<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Bootstrap</title>    
    <!-- Cara install sendiri -->
    <!--<link rel="stylesheet" href="assets/css/bootstrap.min.css">-->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="" class="rel">

    <link rel="stylesheet" href="assets/css/style.css">
    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" >
    
    <style>
    .btn-primary{
        background:yellow;
    }
    </style>

</head>
<body>
    
    <?php include "navbar.php"; ?>
    
    <div class="jumbotron text-center">
        <img src="image/user.png" alt="" class="img-circle">
        <h1>START BOOTSTRAP</h1>
        <p><?php echo strtoupper('Web Developer');?> - Graphic Artist - User Experince Designer</p>
        <p><?php echo date('y-m-d');?></p>

        <button class="btn btn-primary">Contoh</button>
    </div>
    
    <section class="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>PORTFOLIO</h2>
                    <hr>
                </div>
            </div>
            <div class="row image">
                
                <div class="col-md-4 col-md-offset-1 thumbnail">
                    <img  src="image/1.png" alt="">
                </div>
                
                <div class="col-md-4 thumbnail">
                    <img  src="image/2.png" alt="">
                </div>
                
                <div class="col-md-4 thumbnail">
                    <img  src="image/3.png" alt="">
                </div>
                
                <div class="col-md-3 col-md-offset-1 thumbnail">
                    <img  src="image/4.png" alt="">
                </div>
                <div class="col-md-3 thumbnail">
                    <img  src="image/5.png" alt="">
                </div>
                <div class="col-md-3 thumbnail">
                    <img  src="image/6.png" alt="">
                </div>
                
            </div>
            
        </div>
    </section>
    
    <section class="about">
        <div class="container" >
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>ABOUT</h2>
                    <hr>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-5 col-md-offset-1">
                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Adipisci provident repudiandae blanditiis. Deleniti perferendis obcaecati eveniet? Harum sapiente, nobis eligendi saepe qui, tenetur molestias dolor nulla neque reiciendis quia quidem. Lorem ipsum dolor sit, amet consectetur adipisicing elit. Veritatis natus accusantium velit voluptate incidunt excepturi fuga, quisquam, blanditiis sequi a voluptatem odit, quae quaerat soluta. Inventore a non perferendis eveniet?</p>
                </div>
                <div class="col-md-5">
                    <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Optio sit fugit est repellat amet nihil, atque ad veritatis mollitia quasi quis facilis asperiores sed. Unde dicta ipsam sed? Sit, autem! Lorem ipsum, dolor sit amet consectetur adipisicing elit. Odio omnis voluptas sapiente veritatis reiciendis officiis voluptates aliquid, eum nihil accusantium nobis ad odit nam ea minima impedit neque suscipit eos?</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <button type="button" class="btn btn-default">
                        <span class="glyphicon glyphicon-save"></span> Download Now!
                    </button>
                </div>
            </div>
        </div>
    </section>
    
    <section class="contact">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>CONTACT ME</h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form>
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name">
                        </div> 
                        <div class="form-group">
                            <label for="email">Email Address</label>
                            <input type="email" class="form-control" id="email">
                        </div>
                        <div class="form-group">
                            <label for="phone">Phone Number</label>
                            <input type="tel" class="form-control" id="phone">
                        </div> 
                        <div class="form-group">
                            <label for="message">Message</label>
                            <textarea class="form-control" rows="4"></textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Send</button>
                            <a type="button" class="btn-default btn-fb"></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container-fluid">
            <div class="row text-center info">
                <div class="container">
                    <div class="col-md-4">
                        <div>
                            <p>LOCATION</p>
                        </div>
                        <div class="lokasi">
                            <p>2125 John Daniel Drive <br>
                                Clark, MO 65243
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 button">
                        <div>
                            <p>AROUND THE WEB</p>
                        </div>
                        <div>
                            <button type="button" class="btn btn-default btn-circle btn-lg" ><i class="fa fa-facebook-f"></i></button>
                            <button type="button" class="btn btn-default btn-circle btn-lg"><i class="fa fa-google-plus"></i></button>
                            <button type="button" class="btn btn-default btn-circle btn-lg"><i class="fa fa-twitter"></i></button>
                            <button type="button" class="btn btn-default btn-circle btn-lg"><i class="fa fa-linkedin"></i></button>
                            <button type="button" class="btn btn-default btn-circle btn-lg"><i class="fa fa-instagram"></i></button>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div>
                            <p>ABOUT FREELANCER</p>
                        </div>
                        <div class="freelance">
                            <p>Freelance is a free to use,open source <br> Bootstrap theme created by <span>start Bootstrap</span></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="footer">
        <div class="container">
            <div class="row text-center ">
                <div class="col-md-12">
                    <p>Copyright &copy; My Bootstrap 2018</p>
                </div>
            </div>
        </div>
    </section>
    
</body>
</html>