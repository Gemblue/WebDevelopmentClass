-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `agama`;
CREATE TABLE `agama` (
  `id_agama` int(5) NOT NULL AUTO_INCREMENT,
  `nama_agama` varchar(10) NOT NULL,
  PRIMARY KEY (`id_agama`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `agama` (`id_agama`, `nama_agama`) VALUES
(1,	'Islam'),
(2,	'Kristen');

DROP TABLE IF EXISTS `city`;
CREATE TABLE `city` (
  `id_city` int(5) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(30) NOT NULL,
  PRIMARY KEY (`id_city`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `city` (`id_city`, `city_name`) VALUES
(1,	'Bandung'),
(2,	'Jakarta'),
(3,	'Semarang');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `mobile` varchar(13) DEFAULT NULL,
  `id_city` int(5) NOT NULL,
  `id_agama` int(5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `id_city`, `id_agama`) VALUES
(4,	'Jalil',	'jalilsputra@gmail.com',	'087778086140',	1,	2),
(5,	'Tunggul',	'tunggul@gmail.com',	'083366888211',	2,	2),
(21,	'Oriza',	'oriza@ajita.co.id',	'08777777',	1,	0),
(22,	'Sample',	'sample@gmail.com',	'0645677682',	2,	1),
(23,	'Sample2',	'sample2@gmail.com',	'4438117045',	2,	1);

-- 2018-05-19 04:56:46
