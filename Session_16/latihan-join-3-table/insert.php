<?php
// Mengkoneksikan PHP dengan Database MySQL
include('connection.php');

// Menangkap data dari form HTML dan meng-assign ke dalam variabel baru
$name = $_POST['name'];
$email = $_POST['email'];
$mobile = $_POST['mobile'];
$id_city = $_POST['id_city'];
$id_agama = $_POST['id_agama'];

// Melakukan validasi terhadap isi data form.
if ($name == '' || $email == '' || $mobile == '')
{
    echo "Isi semua data!";
    echo '<meta http-equiv="refresh" content="0; url=add.php?message=Isi dulu semua field nya bro!" />';
    exit;
}

// Untuk ngecek email sudah ada apa belum
$query = mysqli_query($connect, "SELECT email FROM users WHERE email = '$email'");
$check_email = mysqli_fetch_array($query);

if (!empty($check_email))
{
    echo '<meta http-equiv="refresh" content="0; url=add.php?message=Email telah dipakai" />';
    exit;
}

// Melakukan input ke database.
$sql = "INSERT INTO users (name, email, mobile, id_city, id_agama) VALUES ('$name', '$email', '$mobile', '$id_city', '$id_agama')";
$results = mysqli_query($connect, $sql);

// Handle, jika berhasil masuk ke DB, lemparkan / redirect kembali ke halaman data.
if ($results == true)
    echo '<meta http-equiv="refresh" content="0; url=index.php" />';
else 
    echo 'Gagal tambah data';