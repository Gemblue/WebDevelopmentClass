<html>
<head>
    <title>Management User</title>
    
    <!-- Install Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
</head>
<body>

<div class="container">

    <h1>Daftar User</h1>
    
    <a href="add.php" class="btn btn-default">Tambah User</a>

    <br/><br/>

    <?php if (isset($_GET['message'])) :?>
        <div class="alert alert-success"><?php echo $_GET['message'];?></div>
    <?php endif;?>

    <table class="table table-striped">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Email</th>
            <th>Mobile</th>
            <th>City</th>
            <th>Agama</th>
            <th>Options</th>
        </tr>

        <?php
        include('connection.php');
        $results = mysqli_query($connect, "SELECT * FROM users JOIN city ON city.id_city = users.id_city 
                                                               JOIN agama ON agama.id_agama = users.id_agama");
        
        $no = 1;
        while($user = mysqli_fetch_array($results))
        {
            ?>
            <tr>
                <td><?php echo $no?></td>
                <td><?php echo $user['name']?></td>
                <td><?php echo $user['email']?></td>
                <td><?php echo $user['mobile']?></td>
                <td><?php echo $user['city_name']?></td>
                <td><?php echo $user['nama_agama']?></td>
                <td>
                    <a class="btn btn-danger btn-xs" href="delete.php?id=<?php echo $user['id']?>">Delete</a>
                    <a class="btn btn-default btn-xs" href="edit.php?id=<?php echo $user['id']?>">Edit</a>
                </td>
            </tr>
            <?php
            $no++;
        }

        ?>

        
    </table>

</div>
    
</body>
</html>