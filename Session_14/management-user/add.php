<html>
<head>
    <title>Management User</title>
    
    <!-- Install Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
</head>
<body>

<div class="container">

    <h1>Form Tambah User</h1>
    
    <?php if (isset($_GET['message'])) :?>
        <div class="alert alert-danger"><?php echo $_GET['message'];?></div>
    <?php endif;?>
    
    <form method="post" action="insert.php">
        <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" class="form-control" required/>
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" class="form-control" required/>
        </div>
        <div class="form-group">
            <label>Mobile</label>
            <input type="text" name="mobile" class="form-control" required/>
        </div>
        <a href="index.php" class="btn btn-default">Kembali ke Daftar</a>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
    

</div>
    
</body>
</html>