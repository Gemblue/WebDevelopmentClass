<html>
<head>
    <title>Management User</title>
    
    <!-- Install Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
</head>
<body>

<div class="container">

    <h1>Form Edit User</h1>
    
    <?php if (isset($_GET['message'])) :?>
        <div class="alert alert-danger"><?php echo $_GET['message'];?></div>
    <?php endif;?>
    
    <?php
    include('connection.php');
    $id = $_GET['id'];
    $result = mysqli_query($connect, "SELECT * FROM users WHERE id = '$id'");
    $previous = mysqli_fetch_array($result);
    ?>

    <form method="post" action="update.php">

        <input type="hidden" name="id" value="<?php echo $previous['id']?>">
        
        <div class="form-group">
            <label>Name</label>
            <input type="text" name="name" value="<?php echo $previous['name']?>" class="form-control" required/>
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="email" name="email" value="<?php echo $previous['email']?>" class="form-control" required/>
        </div>
        <div class="form-group">
            <label>Mobile</label>
            <input type="text" name="mobile" value="<?php echo $previous['mobile']?>" class="form-control" required/>
        </div>
        <a href="index.php" class="btn btn-default">Kembali ke Daftar</a>
        <button type="submit" class="btn btn-primary">Simpan</button>
    </form>
    

</div>
    
</body>
</html>