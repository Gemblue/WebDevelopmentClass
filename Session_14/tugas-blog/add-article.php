<!DOCTYPE html>
<html>
<head>
	<title>article</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">
	<h3>FORM ARTICLE</h3>
	<form action="insert.php" method="POST">
	<div class="form-group">
		<label>Tanggal</label>
		<input type="date" name="tanggal" class="form-control" required autofocus="" value="<?php echo date('Y-m-d');?>">
	</div>
	<div class="form-group">
		<label>Judul</label>
		<input type="text" name="judul" class="form-control" required>
	</div>
	<div class="form-group">
		<label>Deskripsi</label>
		<input type="text" name="deskripsi" class="form-control" required>
	</div>
	<div class="form-group">
		<label>Upload Image</label>
		<input type="text" name="image" class="form-control" required>
	</div>
	<button class="btn btn-primary" name="submit" type="submit">Simpan</button>
	</form>
</div>
</body>
</html>