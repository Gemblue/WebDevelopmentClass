/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : _blog

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2018-05-11 21:22:14
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `articles`
-- ----------------------------
DROP TABLE IF EXISTS `articles`;
CREATE TABLE `articles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `judul` varchar(50) DEFAULT NULL,
  `deskripsi` text,
  `image` varchar(255) DEFAULT NULL,
  `status` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of articles
-- ----------------------------
INSERT INTO `articles` VALUES ('1', '2018-05-05', 'RD memberikan Klarifikasi', 'Sriwijaya vs BU menjadi pertandngan yang di tunggu tunggu, ', 'http://cdn2.tstatic.net/jogja/foto/bank/images/sfc-vs-butd_20180211_185829.jpg', 'OPEN');
INSERT INTO `articles` VALUES ('2', '2018-05-05', 'Persib Kalah lagi!', 'Persib mengalamai kekalahan dalam laga melawan PSMS medan dengan Skor 1-0', 'https://asset.indosport.com/article/image/q/80/182282/persib_bandung-169.jpg?w=750&h=423', 'OPEN');
INSERT INTO `articles` VALUES ('3', '2018-05-05', 'Timnas U19 bernafas lega!', 'Timnas menang melawan Bahrain, mimipi indra safri  didepan mata', 'http://cdn2.tstatic.net/jateng/foto/bank/images/timnas-u-19_20171125_181501.jpg', 'OPEN');
INSERT INTO `articles` VALUES ('4', '2018-05-07', 'Kesuksean Luis Mia', 'Luis Mia Memalukan Indonesia dihadapan lawan', 'http://cdn2.tstatic.net/jateng/foto/bank/images/timnas-u-19_20171125_181501.jpg', 'OPEN');
INSERT INTO `articles` VALUES ('5', '2018-05-06', 'Bhayangkara Siap', 'Dalam rangka menyambut bulan suci.. bla blabla bla bla bla bla bla', 'https://images.performgroup.com/di/library/GOAL/97/6c/bhayangkara-fc_18hkv0gbeqgwr11efkwqqve4fc.jpg?t=-1620296889&quality=90&w=1280', 'OPEN');
