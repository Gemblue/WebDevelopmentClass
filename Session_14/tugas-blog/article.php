<!DOCTYPE html>
<html>
<head>
	<title>article</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">
	<h3>ARTICLES</h3>
	<a href="index.php" class="btn btn-default">Dashboard</a>
	<a href="add-article.php" class="btn btn-primary">Add New</a>
	<br/>
	<br/>
	<table class="table table-striped table-bordered">
		<tr>
			<th>ID</th>
			<th>TANGGAL</th>
			<th>JUDUL</th>
			<th>DESKRIPSI</th>
			<th>IMAGE</th>
			<th colspan="2">AKSI</th>
		</tr>

		<?php
			include('koneksi.php');
			$sql = "select * from articles";
			$results = mysqli_query($conn, $sql);

			while ($article = mysqli_fetch_array($results)) {
				?>
					<tr>
						<td><?php echo $article['id'];?></td>
						<td><?php echo $article['tanggal'];?></td>
						<td><?php echo $article['judul'];?></td>
						<td><?php echo $article['deskripsi'];?></td>
						<td><?php echo $article['image'];?></td>
						<td><a href="update.php">Update</a></td>
						<td><a href="delete.php">Delete</a></td>
					</tr>

				<?php
				# code...
			}
		?>
	</table>
</div>
</body>
</html>