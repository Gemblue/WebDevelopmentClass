<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Role_model extends CI_Model {

    // Ngambil roles
    public function getRoles()
    {
        $this->db->select('*');
        $this->db->from('role');
        
        return $this->db->get()->result_array();
    }
}