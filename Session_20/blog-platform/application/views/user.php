<?php $this->load->view('header');?>

<div class="container">
	<h1>Halaman Data User</h1>
    
    <?php echo $this->session->flashdata('message');?>

    <br/><br/>

    <?php $this->load->view('menu');?>

    <br/><br/>

    <a href="<?php echo site_url('index.php/user/add');?>">New User</a>
    
    <br/><br/>
    
    <form action="<?php echo site_url('index.php/user/search');?>" method="get">
        <div class="input-group">
        <input type="text" name="name" class="form-control" placeholder="Search for..."]>
        <span class="input-group-btn">
            <button class="btn btn-default" type="submit">Go!</button>
        </span>
        </div><!-- /input-group -->
    </form>
    
    <br/><br/>

    <table class="table table-bordered">
        <tr>
            <th>Name</th>
            <th>Username</th>
            <th>Email</th>
            <th>Role</th>
            <th>Created at</th>
            <th>Updated at</th>
            <th>Option</th>
        </tr>
        
        <?php foreach($users as $user) :?>
            <tr>
                <td><?php echo $user['name'];?></td>
                <td><?php echo $user['username'];?></td>
                <td><?php echo $user['email'];?></td>
                <td><?php echo $user['role_name'];?></td>
                <td><?php echo $user['created_at'];?></td>
                <td><?php echo $user['updated_at'];?></td>
                <td>
                    <a href="<?php echo site_url('index.php/user/delete/' . $user['id']);?>">Delete</a>
                    <a href="<?php echo site_url('index.php/user/edit/' . $user['id']);?>">Edit</a>
                </td>
            </tr>
        <?php endforeach;?>

    </table>

    <?php if (!empty($links)):?>
        <div class="pagination">
            <?php echo $links;?>
        </div>
    <?php endif;?>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. </p>
</div>

<?php $this->load->view('footer');?>