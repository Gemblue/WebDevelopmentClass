<?php $this->load->view('header');?>

<div class="container">
	<h1>Form Blog</h1>

    <?php echo $this->session->flashdata('message');?>

    <!-- Define action -->
    <?php 
    if (!empty($previous['title']))
        $action = site_url('index.php/blog/update');
    else
        $action = site_url('index.php/blog/insert');
    ?>

    <form action="<?php echo $action;?>" method="post" enctype="multipart/form-data">

        <input type="hidden" name="id" value="<?php echo (!empty($previous['id'])) ? $previous['id'] : '';?>" /> 
        
        <div class="form-group">
            <label for="">Title</label>
            <input type="text" id="title" name="title" class="form-control" value="<?php echo (!empty($previous['title'])) ? $previous['title'] : '';?>"/>
        </div>

        <div class="form-group">
            <label for="">Schedule</label>
            <input type="date" name="" class="form-control datepicker"/>
        </div>

        <div class="form-group">
            <label for="">Featured Image</label>
            <input type="file" id="feature_image" name="featured_image" class="form-control"/>
        </div>

        <div class="form-group">
            <label for="">Description</label>
            <input type="text" id="description" name="description" class="form-control" value="<?php echo (!empty($previous['description'])) ? $previous['description'] : '';?>"/>
        </div>
        
        <div class="form-group">
            <label for="">Content</label>
            <textarea name="content" id="content" class="form-control"/><?php echo (!empty($previous['content'])) ? $previous['content'] : '';?></textarea>
        </div>
        
        <div class="form-group">
            <label for="">Author</label>
            <input type="text" id="author" name="author" class="form-control" value="<?php echo (!empty($previous['author'])) ? $previous['author'] : '';?>"/>
        </div>
        
        <a href="<?php echo base_url('index.php/blog')?>" class="btn btn-default">Back</a>
        <button type="submit" class="btn btn-success">Save</button>
    </form>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. </p>
</div>

<?php $this->load->view('footer');?>