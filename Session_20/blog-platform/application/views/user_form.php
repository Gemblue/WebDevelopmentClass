<?php $this->load->view('header');?>

<div class="container">
	<h1>Halaman New User</h1>

    <?php echo $this->session->flashdata('message');?>

    <!-- Define action -->
    <?php 
    if (!empty($previous['id']))
        $action = site_url('index.php/user/update');
    else
        $action = site_url('index.php/user/insert');
    ?>

    <form action="<?php echo $action;?>" method="post">

        <input type="hidden" name="id" value="<?php echo (!empty($previous['id'])) ? $previous['id'] : '';?>" /> 

        <div class="form-group">
            <label for="">Name</label>
            <input type="text" name="name" class="form-control" value="<?php echo (!empty($previous['name'])) ? $previous['name'] : '';?>"/>
        </div>

        <div class="form-group">
            <label for="">Username</label>
            <input type="text" name="username" class="form-control" value="<?php echo (!empty($previous['username'])) ? $previous['username'] : '';?>"/>
        </div>

        <div class="form-group">
            <label for="">Role</label>
            <select name="role_id" class="form-control">
                <?php foreach($roles as $role):?>
                    <option value="<?php echo $role['id'];?>" <?php echo ($role['id'] == $previous['role_id']) ? 'selected' : '';?>><?php echo $role['role_name'];?></option>
                <?php endforeach;?>
            </select>
        </div>
        
        <div class="form-group">
            <label for="">Email</label>
            <input name="email" class="form-control" value="<?php echo (!empty($previous['email'])) ? $previous['email'] : '';?>" />
        </div>
        
        <div class="form-group">
            <label for="">Password</label>
            <input type="password" name="password" class="form-control"/>
        </div>

        <a href="<?php echo base_url('index.php/user')?>" class="btn btn-default">Back</a>
        <button type="submit" class="btn btn-success">Save</button>
    </form>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. </p>
</div>

<?php $this->load->view('footer');?>