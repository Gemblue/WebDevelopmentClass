<?php $this->load->view('header');?>

<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <h1>Login</h1>
            <?php echo $this->session->flashdata('message');?>
            <form action="<?php echo site_url('index.php/permission/login');?>" method="post">
                <div class="form-group">
                    <label for="">Username</label>
                    <input type="text" name="username" class="form-control"/>
                </div>        
                <div class="form-group">
                    <label for="">Password</label>
                    <input type="password" name="password" class="form-control"/>
                </div>
                <button type="submit" class="btn btn-success">Login</button>
            </form>
        </div>
    </div>
</div>

<?php $this->load->view('footer');?>