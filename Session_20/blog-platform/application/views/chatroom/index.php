<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Menit.Com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <style>
    body{
        font-family:verdana;
    }

    form{
        margin-bottom:20px;
    }
    </style>
</head>
<body>

    <div class="container">

        <h1>Menit.Com</h1>
        
        <form>
            <div class="form-group">
                <label for="">Chat</label>
                <textarea name="chat" id="chat" cols="5" rows="3" class="form-control"></textarea>
            </div>
            <button type="button" class="btn btn-success" id="btn-send">Send</button>
        </form>

        <div class="chat-room"></div>

    </div>

    <!-- Load Js -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    
    <!-- Bridge -->
    <input type="hidden" id="base_url" value="<?php echo base_url()?>">

    <script>
    var base_url = $('#base_url').val();

    $('#chat').keypress(function (e) {
        if(e.which ==13)
        {
            send();
        }
    });

    /**
     * Mengirim data chat!
     */
    $('#btn-send').click(function(){
        send();
    })

    /**
     * Mengambil data chat dari server berbentuk JSON
     */
    $.getJSON( base_url + "index.php/chatroom/getChats", function( data ) {
        $.each( data, function( key, val ) {
            $('.chat-room').prepend('<div class="well">'+ val.chat + '<br/>' + val.created_at +'</div>');
        });
    });

    // Supaya bisa dipanggil dimana mana
    function send()
    {
        var input_chat = $('#chat').val();
        
        $.post( base_url + "index.php/chatroom/insert", { chat: input_chat })
        .done(function( response ) {

            if (response == 'Done!')
            {
                $('#chat').val('');

                var html = '<div class="well">'+ input_chat + '<br/>' + getDate() +'</div>';
                
                $('.chat-room').prepend(html);
            }
            else
            {
                alert(response);
            }
        });
    }
    
    function getDate()
    {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1;
        var yyyy = today.getFullYear();
        var h = today.getHours();
        var i = today.getMinutes();
        var s = today.getSeconds(); 
        
        return yyyy + '-' + mm + '-' + dd + ' ' + h + '-' + i + '-' + s;

    }
    </script>

</body>
</html>