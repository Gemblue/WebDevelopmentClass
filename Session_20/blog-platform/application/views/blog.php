<?php $this->load->view('header');?>

<div class="container">
	<h1>Halaman Data Blog</h1>
    
    <?php echo $this->session->flashdata('message');?>

    <br/><br/>

    <?php $this->load->view('menu');?>

    <br/><br/>

    <a href="<?php echo site_url('index.php/blog/add');?>">New blog</a>
    
    <br/><br/>
    
    <form action="<?php echo site_url('index.php/blog/search');?>" method="get">
        <div class="input-group">
        <input type="text" name="title" class="form-control" placeholder="Search for..."]>
        <span class="input-group-btn">
            <button class="btn btn-default" type="submit">Go!</button>
        </span>
        </div><!-- /input-group -->
    </form>
    
    <br/><br/>

    <table class="table table-bordered">
        <tr>
            <th>Title</th>
            <th>Description</th>
            <th>Content</th>
            <th>Author</th>
            <th>Featured Image</th>
            <th>Created at</th>
            <th>Updated at</th>
            <th>Option</th>
        </tr>
        
        <?php foreach($blogs as $blog) :?>
            <tr>
                <td><?php echo $blog['title'];?></td>
                <td><?php echo $blog['description'];?></td>
                <td><?php echo word_limiter($blog['content'], 20, ' ..');?></td>
                <td><?php echo $blog['author'];?></td>
                <td>
                    <!-- Cek kalau gambar kosong! -->
                    <?php if (empty($blog['featured_image'])) :?>
                        No featured image ..                     
                    <?php else:?>
                        <img width="200" src="<?php echo site_url('uploads/' . $blog['featured_image']);?>"/></td>
                    <?php endif;?>
                <td><?php echo $blog['created_at'];?></td>
                <td><?php echo $blog['updated_at'];?></td>
                <td>
                    <a href="<?php echo site_url('index.php/blog/delete/' . $blog['id']);?>">Delete</a>
                    <a href="<?php echo site_url('index.php/blog/edit/' . $blog['id']);?>">Edit</a>
                </td>
            </tr>
        <?php endforeach;?>

    </table>

    <div class="pagination">
        <?php echo $links;?>
    </div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. </p>
</div>

<?php $this->load->view('footer');?>