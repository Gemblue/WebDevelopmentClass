<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();

        // Membuat pengecekan izin akses ..
        $role_name = $this->session->userdata('role_name');

        if ($role_name != 'Admin')
        {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">Silahkan login dulu untuk masuk</div>');
            redirect('index.php/permission');
        }

        // Load resources
        $this->load->helper('text');
        $this->load->helper('string');
        $this->load->model('blog_model');
    }

    // Menampilkan data blog yang ada di DB
    public function index($page = 0)
	{   
        $this->load->library('pagination');

        $config['base_url'] = base_url('index.php/blog/index');
        $config['total_rows'] = $this->blog_model->getTotalBlogs();
        $config['per_page'] = 5;
        
        $this->pagination->initialize($config);

        $data['links'] = $this->pagination->create_links();
        $data['blogs'] = $this->blog_model->getBlogs($config['per_page'], $this->uri->segment(3));
        
        $this->load->view('blog', $data);
    }
    
    public function search()
	{
        $title = $this->input->get('title');

        $data['blogs'] = $this->blog_model->searchBlogs($title);
        
        $this->load->view('blog', $data);
    }
    
    public function add()
    {
        $this->load->view('blog_form');
    }

    public function edit($id)
    {
        $data['previous'] = $this->blog_model->getDetail($id);

        $this->load->view('blog_form', $data);
    }

    public function update()
    {
        // Mengambil data form.
        $id = $this->input->post('id', true);
        $title = $this->input->post('title', true);
        $description = $this->input->post('description', true);
        $content = $this->input->post('content', true);
        $author = $this->input->post('author', true);

        // Handling upload files ..
        $this->load->library('upload', [
            'upload_path'         => './uploads/',
            'allowed_types'       => 'gif|jpg|png',
            'max_size'            => 1000,
            'max_width'           => 1024,
            'max_height'          => 768,
            'file_name'           => random_string('alnum', 10)
        ]);

        $upload = $this->upload->do_upload('featured_image');
        
        if ($upload == true)
        {
            $file = $this->upload->data();
            $file_name = $file['file_name'];
        }
        else 
        {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">'. $this->upload->display_errors() .'</div>');
            redirect('index.php/blog');
        }

        // Membuat data array
        $param = [
            'title' => $title,
            'description' => $description,
            'content' => $content,
            'author' => $author,
            'featured_image' => $file_name
        ];

        // Memanggil query ke DB untuk update data.
        $this->blog_model->updateBlog($id, $param);

        $this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil update data</div>');
        redirect('index.php/blog');
    }

    public function insert()
    {
        // Mengambil data form.
        $title = $this->input->post('title', true);
        $description = $this->input->post('description', true);
        $content = $this->input->post('content', true);
        $author = $this->input->post('author', true);

        // Panggil library
        $this->load->library('form_validation');

        // Set validasi
        $this->form_validation->set_rules('title', 'Title', 'required')
                              ->set_rules('description', 'Description', 'required')
                              ->set_rules('content', 'Content', 'required|min_length[10]')
                              ->set_rules('author', 'Author', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">' . validation_errors() . '</div>');
            redirect('index.php/blog/add');
        }

        // Membuat data array
        $param = [
            'title' => $title,
            'description' => $description,
            'content' => $content,
            'author' => $author
        ];

        // Memanggil query ke DB untuk insert data.
        $this->blog_model->insertBlog($param);

        $this->session->set_flashdata('message', 'Berhasil input data!');
        redirect('index.php/blog');
    }

    public function insert_ajax()
    {
        // Mengambil data form.
        $title = $this->input->post('title', true);
        $description = $this->input->post('description', true);
        $content = $this->input->post('content', true);
        $author = $this->input->post('author', true);
        
        // Membuat data array
        $param = [
            'title' => $title,
            'description' => $description,
            'content' => $content,
            'author' => $author
        ];
        
        // Memanggil query ke DB untuk insert data.
        $this->blog_model->insertBlog($param);

        echo 'Ok!';
    }

    public function delete($id)
    {
        $this->blog_model->delete($id);
        
        $this->session->set_flashdata('message', 'Berhasil delete data!');
        redirect('index.php/blog');
    }
}