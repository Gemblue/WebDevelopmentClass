<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Chatroom extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {   
        $this->load->view('chatroom/index');
    }

    public function insert()
    {
        $chat = $this->input->post('chat');

        // Panggil library
        $this->load->library('form_validation');
        
        // Set validasi
        $this->form_validation->set_rules('chat', 'Chat', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            echo validation_errors();
            exit;
        }

        $insert = $this->db->insert('chat', [
            'chat' => $chat,
            'created_at' => date('Y-m-d H:i:s')
        ]);

        if ($insert == true)
        {
            echo 'Done!';
        }
        else 
        {
            echo 'Failed!';
        }
    }

    public function getChats()
    {
        $this->db->select('*');
        $this->db->from('chat');
        $results = $this->db->get()->result_array();
        
        header('Content-Type: application/json');
        echo json_encode($results);
    }
}