<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();

        // Load resources
        $this->load->helper('text');
        $this->load->model('blog_model');
    }

    public function index()
    {   
        $data['blogs'] = $this->blog_model->getBlogs(10, 0); 

        $this->load->view('site/homepage', $data);
    }

    public function detail($id)
    {
        $data['blog'] = $this->blog_model->getDetail($id);

        $this->load->view('site/detail', $data);
    }
}