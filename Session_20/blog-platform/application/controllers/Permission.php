<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Permission extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();

        $this->load->model('User_model');
    }

    // Tampilkan halaman login!
    public function index()
    {
        $this->load->view('login');
    }

    public function logout()
    {
        $this->session->sess_destroy();

        redirect('index.php/permission');
    }

    public function login()
    {
        // Tangkap data dari form
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        // Logika pengecekan data ..
        $login = $this->User_model->login($username, $password);

        if ($login == true)
        {
            redirect('index.php/blog');
        }

        $this->session->set_flashdata('message', '<div class="alert alert-danger">Gagal login, username dan password tidak terdaftar</div>');
        redirect('index.php/permission');
    }
}