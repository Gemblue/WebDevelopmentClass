<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set("Asia/Bangkok");

        // Membuat pengecekan izin akses ..
        $role_name = $this->session->userdata('role_name');

        if ($role_name != 'Admin')
        {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">Silahkan login dulu untuk masuk</div>');
            redirect('index.php/permission');
        }

        $this->load->helper('text');
        $this->load->model('user_model');
        $this->load->model('role_model');
    }

    // Menampilkan data user yang ada di DB
    public function index($page = 0)
	{   
        $this->load->library('pagination');

        $config['base_url'] = base_url('index.php/user/index');
        $config['total_rows'] = $this->user_model->getTotalUsers();
        $config['per_page'] = 5;
        
        $this->pagination->initialize($config);

        $data['links'] = $this->pagination->create_links();
        $data['users'] = $this->user_model->getUsers($config['per_page'], $this->uri->segment(3));
        
        $this->load->view('user', $data);
    }
    
    public function search()
	{
        $name = $this->input->get('name');

        $data['users'] = $this->user_model->searchUsers($name);
        
        $this->load->view('user', $data);
    }
    
    public function add()
    {
        $data['roles'] = $this->role_model->getRoles();

        $this->load->view('user_form', $data);
    }

    public function edit($id)
    {
        $data['previous'] = $this->user_model->getDetail($id);
        $data['roles'] = $this->role_model->getRoles();

        $this->load->view('user_form', $data);
    }

    public function update()
    {
        // Mengambil data form.
        $id = $this->input->post('id', true);
        $username = $this->input->post('username', true);
        $name = $this->input->post('name', true);
        $email = $this->input->post('email', true);
        $password = $this->input->post('password', true);
        $role_id = $this->input->post('role_id', true);

        // Membuat data array
        $param = [
            'username' => $username,
            'name' => $name,
            'email' => $email,
            'role_id' => $role_id,
            'password' => md5($password),
            'updated_at' => date('y-m-d h:i:s')
        ];

        // Memanggil query ke DB untuk update data.
        $this->user_model->updateUser($id, $param);

        $this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil update data</div>');
        redirect('index.php/user');
    }

    public function insert()
    {
        // Mengambil data form.
        $name = $this->input->post('name', true);
        $username = $this->input->post('username', true);
        $email = $this->input->post('email', true);
        $password = $this->input->post('password', true);
        $role_id = $this->input->post('role_id', true);

        // Panggil library
        $this->load->library('form_validation');

        // Set validasi
        $this->form_validation->set_rules('role_id', 'Role', 'required')
                              ->set_rules('name', 'Name', 'required')
                              ->set_rules('username', 'Username', 'required|is_unique[users.username]')
                              ->set_rules('email', 'Email', 'required|min_length[10]|valid_email|is_unique[users.email]')
                              ->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('message', '<div class="alert alert-danger">' . validation_errors() . '</div>');
            redirect('index.php/user/add');
        }

        // Membuat data array
        $param = [
            'name' => $name,
            'username' => $username,
            'email' => $email,
            'password' => md5($password),
            'role_id' => $role_id,
            'created_at' => date('y-m-d h:i:s')
        ];

        // Memanggil query ke DB untuk insert data.
        $this->user_model->insertUser($param);
        
        $this->session->set_flashdata('message', 'Berhasil input data!');
        redirect('index.php/user');
    }

    public function delete($id)
    {
        $this->blog_model->delete($id);
        
        $this->session->set_flashdata('message', 'Berhasil delete data!');
        redirect('index.php/blog');
    }
}