-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `slug` varchar(255) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` tinytext NOT NULL,
  `featured_image` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `author` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `blog` (`id`, `slug`, `title`, `description`, `featured_image`, `content`, `author`, `created_at`, `updated_at`) VALUES
(1,	'samsung-ingin-seluruh-produknya-pakai-ai',	'Samsung Ingin Seluruh Produknya Pakai AI',	'Samsung Ingin Seluruh Produknya Pakai AI DDD',	'Yo0VGTO9yJ.png',	'Jakarta - Samsung berencana untuk memperluas penggunaan kecerdasan buatan (AI) di produk-produknya. Rencana itu akan melibatkan asisten digitalnya, Bixby.\r\n\r\nVendor asal Korea Selatan ini memang tidak hanya terkenal dengan produk smartphonenya. Ada beragam perangkat yang telah diproduksi Samsung mulai dari televisi pintar hingga kulkas. Tiap tahunnya, Samsung mampu menjual hingga setengah miliar unit. Nah rencananya pada 2020 mendatang, semua perangkat besutan mereka akan dibekali Bixby.',	'admin',	'2018-05-26 13:28:21',	'2018-05-26 13:28:21'),
(7,	'',	'Bala bala',	'Enim voluptates officia ipsum dicta et laboriosam est enim ea officia ex consectetur ut est',	'',	'Labore eius irure voluptatum ut Nam dolorem dolorem labore adipisci voluptatem consequuntur enim eveniet fugiat id eaLabore eius irure voluptatum ut Nam dolorem dolorem labore adipisci voluptatem consequuntur enim eveniet fugiat id eaLabore eius irure voluptatum ut Nam dolorem dolorem labore adipisci voluptatem consequuntur enim eveniet fugiat id eaLabore eius irure voluptatum ut Nam dolorem dolorem labore adipisci voluptatem consequuntur enim eveniet fugiat id eaLabore eius irure voluptatum ut Nam dolorem dolorem labore adipisci voluptatem consequuntur enim eveniet fugiat id eaLabore eius irure voluptatum ut Nam dolorem dolorem labore adipisci voluptatem consequuntur enim eveniet fugiat id eaLabore eius irure voluptatum ut Nam dolorem dolorem labore adipisci voluptatem consequuntur enim eveniet fugiat id ea\r\n',	'Nihil sit unde voluptate et',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00'),
(10,	'',	'Enim excepturi reiciendis excepteur sequi praesentium quo molestiae quis est vitae autem veniam adipisci est ut ipsa dolorem',	'Excepturi autem vero minus tempore a sunt sit perspiciatis',	'',	'Sit dolore expedita eaque beatae est excepturi amet explicabo Ab veritatis sint molestias iusto id omnis odio vero eiusmod rerum',	'Harum soluta non quis quis qua',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00'),
(11,	'',	'Quasi illum eos ex voluptatem quaerat',	'Irure dolor labore saepe earum vero nisi dolorem reprehenderit corrupti explicabo Aut repellendus',	'',	'Non enim ex praesentium blanditiis deserunt minima alias error ut sit magna explicabo Tempor reiciendis adipisicing',	'Nam officiis et numquam exerci',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00'),
(12,	'',	'Voluptatibus odit consequatur ipsum rerum commodi possimus do adipisicing quo reiciendis dolore',	'Obcaecati rem reiciendis ullam ad amet Nam fugit et earum',	'',	'Iste tempor molestias aliquip magni accusantium Nam vel sint voluptas nihil deserunt quia ullamco explicabo Incididunt',	'Est dolores et quia minus temp',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00'),
(13,	'',	'Deserunt fugiat quis modi est in occaecat dolore maiores debitis consectetur proident ab temporibus eum laborum Sed',	'Mollit lorem dolores saepe pariatur Non perferendis nemo id elit et minima corporis voluptate non ratione voluptas magna',	'',	'Adipisci consequatur Debitis non qui exercitationem iste aliqua Sapiente ab amet molestias Nam ut',	'Aute amet ex sit autem sapient',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00'),
(14,	'',	'Tempora non commodo laborum Asperiores excepturi provident est laudantium',	'Porro ut est vitae ut aut commodo minima officia aliquip sapiente duis pariatur Ut dolorem velit rerum velit',	'',	'Amet ullam obcaecati voluptas quibusdam nulla eaque vitae qui eligendi aut nulla adipisicing minima dolorum reprehenderit',	'Duis magni laudantium quia id ',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00');

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(30) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `role` (`id`, `role_name`, `created_at`, `updated_at`) VALUES
(1,	'Admin',	'2018-06-30 11:15:13',	NULL),
(2,	'Subscriber',	NULL,	NULL);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `username` varchar(15) DEFAULT NULL,
  `name` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `role_id` int(5) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` (`id`, `username`, `name`, `email`, `role_id`, `password`, `created_at`, `updated_at`) VALUES
(1,	'sofian',	'Sofian',	'sofian@google.com',	1,	'827ccb0eea8a706c4c34a16891f84e7b',	'2018-06-30 09:55:25',	'2018-06-30 03:07:56'),
(2,	'fahmi',	'Fahmi',	'fahmi@gmail.com',	2,	'827ccb0eea8a706c4c34a16891f84e7b',	'2018-06-30 11:08:33',	'2018-06-30 01:01:45'),
(3,	'marendra',	'Marendra',	'marendra@gmail.com',	2,	'827ccb0eea8a706c4c34a16891f84e7b',	'2018-06-30 01:10:07',	NULL),
(4,	'toni',	'Toni',	'toni@gmail.com',	2,	'827ccb0eea8a706c4c34a16891f84e7b',	'2018-06-30 01:11:34',	NULL);

-- 2018-07-21 02:16:29
