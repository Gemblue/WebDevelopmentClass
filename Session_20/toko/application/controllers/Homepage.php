<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Product_model');
    }

    public function index()
	{   
        $data['products'] = $this->Product_model->getProducts();

        $this->load->view('site/homepage', $data);
    }
}