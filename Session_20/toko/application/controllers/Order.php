<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {
    
    public function __construct()
    {
        parent::__construct();

        date_default_timezone_set("Asia/Bangkok");
        
        $this->load->model('Order_model');
    }

    public function buy()
	{
        $id_user = 1;

        $this->Order_model->insertOrder([
            'id_user' => $id_user,
            'id_product' => $this->input->post('id_product'),
            'qty' => $this->input->post('qty')
        ]);
        
        $this->session->set_flashdata('message', '<div class="alert alert-success">Berhasil order '. $this->input->get('name') .' ..</div>');
        
        redirect($_SERVER['HTTP_REFERER']);
    }
}