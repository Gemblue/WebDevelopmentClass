<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>TokoOnline.Com</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/themes/blue/pace-theme-minimal.min.css">
    
    <style>
    .carousel {
        margin-top:40px;
        margin-bottom:30px;
    }

    .carousel-inner img{
        width:100%;
    }

    .slogan{
        margin-bottom:40px;
    }
    </style>
</head>
<body>

    <div class="container">

        <h1>TokoOnline.Com</h1>

        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <a href="#" data-toggle="modal" data-target="#myModal">
                        <img src="http://via.placeholder.com/800x200" alt="...">
                    </a>
                    
                    <div class="carousel-caption">
                    Lorem ipsum dolor 
                    </div>
                </div>
                <div class="item">
                    <img src="http://via.placeholder.com/800x200" alt="..." id="secondSlide">
                    
                    <div class="carousel-caption">
                    Lorem ipsum dolor
                    </div>
                </div>
            </div>

            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <?php echo $this->session->flashdata('message');?>

        <div class="row">
            <?php foreach($products as $product):?>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <img class="gallery" src="http://via.placeholder.com/300x300" alt="...">
                        <div class="caption">
                            <h3><?php echo $product['name'];?></h3>
                            <p><a href="#" class="btn btn-primary btn-order" data-id="<?php echo $product['id'];?>" data-name="<?php echo $product['name'];?>" data-price="<?php echo $product['price'];?>">Beli</a></p>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>

    <footer>
        <!-- Modal -->
        <div id="myOrder" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <form action="<?php echo base_url('index.php/order/buy')?>" method="post">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Order</h4>
                        </div>
                        <div class="modal-body">
                        
                            <input type="hidden" id="id" name="id_product" />
                            <div class="form-group">
                                <label for="">Name</label>
                                <input type="text" class="form-control" id="name" name="name" disabled/>
                            </div>
                            <div class="form-group">
                                <label for="">Price</label>
                                <input type="text" class="form-control" id="price" name="price" disabled/>
                            </div>
                            <div class="form-group">
                                <label for="">Qty</label>
                                <input type="number" id="qty" class="form-control" name="qty"/>
                            </div>
                            
                            <h5>Total: <span id="total"></span></h5>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success">Beli</button>
                        </div>
                    </form>
                </div>
                

            </div>
        </div>

        <!-- Load Js -->
        <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="<?php echo site_url('assets/js/bootstrap-tooltip.js')?>"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/typed.js/2.0.8/typed.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js"></script>

        <script>
        $(document).ready(function(){

            var price;

            $('.btn-order').click(function(){
                var id = $(this).attr('data-id');
                var name = $(this).attr('data-name');
                
                // Global inject!.
                price = $(this).attr('data-price');
                
                $('#id').val(id);
                $('#name').val(name);
                $('#price').val(price);
                
                $('#myOrder').modal();
                
                return false;
            });

            $('#qty').change(function(){
                
                var qty = $(this).val();
                
                var total = qty * price;
                
                console.log(total);
                    
                $('#total').html('Rp.' +  total + ' ,-');
            })

        });
        </script>
       
    
    </footer>


</body>
</html>