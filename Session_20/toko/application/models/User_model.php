<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

    // Login
    public function login($username, $password)
    {
        // Hash
        $password = md5($password);

        // Logika pengecekan.
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('role', 'role.id = users.role_id');
        $this->db->where('users.username', $username);
        $this->db->where('users.password', $password);

        $user = $this->db->get()->row_array();
        
        if (!empty($user))
        {
            // Generate session.
            $this->session->set_userdata($user);

            return true;
        }

        return false;
    }

    // Ngambil data.
    public function getUsers($limit, $order)
    {
        $this->db->select('*, users.id as id, users.created_at as created_at, users.updated_at as updated_at');
        $this->db->from('users');
        $this->db->join('role', 'role.id = users.role_id');
        $this->db->limit($limit, $order);
        
        return $this->db->get()->result_array();
    }

    // Ngambil data total
    public function getTotalUsers()
    {
        $this->db->select('*');
        $this->db->from('users');
        
        return $this->db->get()->num_rows();
    }

    // Ngambil data search
    public function searchUsers($name)
    {
        // Query Builder.
        $this->db->select('*');
        $this->db->from('users');
        $this->db->join('role', 'role.id = users.role_id');
        $this->db->like('name', $name);
        
        return $this->db->get()->result_array();
    }
    
    public function getDetail($id)
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('id', $id);

        return $this->db->get()->row_array();
    }

    // Insert data.
    public function insertUser($param)
    {
        return $this->db->insert('users', $param);
    }

    public function updateUser($id, $param)
    {
        $this->db->where('id', $id);

        return $this->db->update('users', $param);
    }

    // Delete data
    public function delete($id)
    {
        $this->db->where('id', $id);
        
        return $this->db->delete('users');
    }
}