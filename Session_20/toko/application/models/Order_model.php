<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_model extends CI_Model {

    public function getOrder()
    {
        $this->db->select('*');
        $this->db->from('order');
        
        return $this->db->get()->result_array();
    }

    // Ngambil data total
    public function getTotalOrder()
    {
        $this->db->select('*');
        $this->db->from('order');
        
        return $this->db->get()->num_rows();
    }

    // Ngambil data search
    public function searchOrder($title)
    {
        // Query Builder.
        $this->db->select('*');
        $this->db->from('order');
        $this->db->like('title', $title);
        
        return $this->db->get()->result_array();
    }

    public function getDetail($id)
    {
        $this->db->select('*');
        $this->db->from('order');
        $this->db->where('id', $id);

        return $this->db->get()->row_array();
    }
    
    // Insert data.
    public function insertOrder($param)
    {
        return $this->db->insert('order', $param);
    }

    public function updateorder($id, $param)
    {
        $this->db->where('id', $id);

        return $this->db->update('order', $param);
    }

    // Delete data
    public function delete($id)
    {
        $this->db->where('id', $id);
        
        return $this->db->delete('order');
    }
}