<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {

    public function getProducts()
    {
        $this->db->select('*');
        $this->db->from('products');
        
        return $this->db->get()->result_array();
    }

    // Ngambil data total
    public function getTotalProducts()
    {
        $this->db->select('*');
        $this->db->from('blog');
        
        return $this->db->get()->num_rows();
    }

    // Ngambil data search
    public function searchProducts($title)
    {
        // Query Builder.
        $this->db->select('*');
        $this->db->from('blog');
        $this->db->like('title', $title);
        
        return $this->db->get()->result_array();
    }

    public function getDetail($id)
    {
        $this->db->select('*');
        $this->db->from('blog');
        $this->db->where('id', $id);

        return $this->db->get()->row_array();
    }
    
    // Insert data.
    public function insertBlog($param)
    {
        return $this->db->insert('blog', $param);
    }

    public function updateBlog($id, $param)
    {
        $this->db->where('id', $id);

        return $this->db->update('blog', $param);
    }

    // Delete data
    public function delete($id)
    {
        $this->db->where('id', $id);
        
        return $this->db->delete('blog');
    }
}