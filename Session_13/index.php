<?php
$host = 'localhost';
$database = '_sistem_pendaftaran_siswa';
$username = 'root';
$password = '';
 
$mysqli = mysqli_connect($host, $username, $password, $database);

?>
 
<html>
<head>    
    <title>Homepage</title>
</head>
 
<body> 
    <table border="1">
 
    <tr>
        <th>Nama Lengkap</th> 
        <th>Jenis Kelamin</th> 
        <th>Asal Sekolah</th> 
        <th>Alamat</th>
    </tr>
    <?php
    $results = mysqli_query($mysqli, "SELECT * FROM siswa");
    
    while($result = mysqli_fetch_array($results))
    {
        ?>
        <tr>
            <td><?php echo $result['nama_lengkap'];?></td>
            <td><?php echo $result['jenis_kelamin'];?></td>
            <td><?php echo $result['asal_sekolah'];?></td>
            <td><?php echo $result['alamat'];?></td>
        </tr>
        <?php
    }
    ?>
    </table>
</body>
</html>