<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>WORLD TOP ARTIST</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="https://bootswatch.com/4/darkly/bootstrap.min.css" />

    <style>
    .container{
        padding-top:50px;
    }
    .artist-table{
        margin-top:30px;
    }
    </style>
</head>
<body>

<div class="container">
    <h1>WORLD TOP ARTIST</h1>
    <p>Dalam latihan ini kamu sudah belajar tentang tipe data array dan teknik looping di Bahasa Pemrograman PHP.</p>
    <table class="artist-table table table-striped">
        <tr>
            <th>Nama</th>
            <th>Umur</th>
            <th>Photo</th>
            <th>Tgl Lahir</th>
            <th>Link Profile</th>
            <th>Film yg Dibintangi</th>
        </tr>

        <?php
        $artists = [
            [
                'name' => 'Mark Wahlberg',
                'umur' => 40,
                'tgl_lahir' => '5 Juni 1971',
                'film' => 'Dilwale, Kuch Kuch Hota Hai',
                'photo' => 'https://images-na.ssl-images-amazon.com/images/M/MV5BMjE1MjYwNTE2M15BMl5BanBnXkFtZTYwNTI0NjI1._V1_UX214_CR0,0,214,317_AL_.jpg',
                'link' => 'https://www.instagram.com/iamsrk/?hl=id'
            ],
            [
                'name' => 'Tom Hanks',
                'umur' => 61,
                'tgl_lahir' => '9 Juli 1956',
                'film' => 'Cast Away',
                'photo' => 'https://images-na.ssl-images-amazon.com/images/M/MV5BMTQ2MjMwNDA3Nl5BMl5BanBnXkFtZTcwMTA2NDY3NQ@@._V1_UY317_CR2,0,214,317_AL_.jpg',
                'link' => 'https://www.instagram.com/iamsrk/?hl=id'
            ],
            [
                'name' => 'Johnny Depp',
                'umur' => 54,
                'tgl_lahir' => '9 Juni 1963',
                'film' => 'Cast Away',
                'photo' => 'https://images-na.ssl-images-amazon.com/images/M/MV5BMTM0ODU5Nzk2OV5BMl5BanBnXkFtZTcwMzI2ODgyNQ@@._V1_UY317_CR4,0,214,317_AL_.jpg',
                'link' => 'https://www.instagram.com/iamsrk/?hl=id'
            ]
        ];

        foreach ($artists as $artist)
        {
            ?>
            <tr>
                <td><?php echo $artist['name'];?></td>
                <td><?php echo $artist['umur'];?></td>
                <td><?php echo $artist['tgl_lahir'];?></td>
                <td><?php echo $artist['film'];?></td>
                <td><a class="btn btn-info" href="<?php echo $artist['link'];?>">Profile</a></td>
                <td><img width="100" src="<?php echo $artist['photo'];?>" alt="" /></td>
            </tr>
            <?php
        }
        ?>
        
    </table>

</div>

</body>
</html>