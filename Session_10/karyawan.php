<html>
<head>
    <title>Data Karyawan</title>
</head>
<body>
    <h1>Data Karyawan</h1>
    <table border="1">
        <tr>
            <th>Nama</th>
            <th>Posisi</th>
            <th>Umur</th>
            <th>Lama Kerja</th>
            <th>Jenis Kelamin</th>
            <th>Photo</th>
        </tr>

        <?php
        $karyawan = [
            0 => [
                "nama" => "Budi",
                "posisi" => "Programmer",
                "umur" => "20 Tahun",
                "lama_kerja" => "3 Tahun",
                "jenis_kelamin" => "Pria",
                "photo" => "http://images.detik.com/community/media/visual/2017/09/27/35f1aa21-fc2f-4ae3-9bb1-26cd151d932b.jpg?w=700&q=90"
            ]
        ];

        // Cara standard
        foreach($karyawan as $k)
        {
            ?>
            <tr>
                <td><?php echo $k['nama'];?></td>
                <td><?php echo $k['posisi'];?></td>
                <td><?php echo $k['umur'];?></td>
                <td><?php echo $k['lama_kerja'];?></td>
                <td><?php echo $k['jenis_kelamin'];?></td>
                <td>
                    <?php echo '<img width="100" src="'.$k['photo'].'"  alt="">';?>
                    
                    <img width="100" src="<?php echo $k['photo'];?>" alt="">
                </td>
            </tr>
            <?php
        }

        // Cara gak bener 
        /*
        foreach($karyawan as $k)
        {
            echo "<tr>";
            echo "<td>". $k['nama'] . "</td>";
            echo "<td>". $k['posisi'] . "</td>";
            echo "<td>". $k['umur'] . "</td>";
            echo "<td>". $k['lama_kerja'] . "</td>";
            echo "</tr>";
        }
        */
        ?>
            
    </table>
</body>
</html>