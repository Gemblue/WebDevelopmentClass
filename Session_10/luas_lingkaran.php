<?php

function hitung_lingkaran($jari = 5)
{
    $luas = pi() * ($jari ** 2);

    $result = number_format($luas, 2);

    return $result;
}

echo hitung_lingkaran();


