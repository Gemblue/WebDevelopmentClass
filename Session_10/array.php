<?php
// Macam macam tipe data
$umur = 19; // Integer
$tinggi = 1.7; // Float
$nama = 'Budi'; // String

// Tipe data array / larik / himpunan

// Cara nulis gaya lama
$sayuran = array('kangkung', 'bayam', 'brokoli');

// Cara nulis standard.
$sayuran = [
    'kangkung', 
    'bayam', 
    'brokoli'
];

// Mengakses array spesifik
echo $sayuran[1];

// Menampilkan semua data dalam suatu array
//print_r($sayuran);