<?php
// Custom function
function hitung_luas($panjang = 100, $lebar = 100)
{
    $luas = $panjang * $lebar;
    
    return $luas; 
}

$panjang = 30;
$lebar = 40;
echo hitung_luas($panjang, $lebar);


// Di file lain.
$panjang = 50;
$lebar = 90;

echo hitung_luas($panjang, $lebar);