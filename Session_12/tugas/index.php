<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Katalog Online</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Masang Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <style>
    .image{
        width:100%;
    }
    </style>
</head>
<body>

<div class="container">
    <h1>Katalog Online</h1>

    <div class="row">
        
        <?php
        $content = file_get_contents('produk.json');
        $products = json_decode($content, true);

        foreach($products as $product)
        {
            ?>
            
            <!-- Mulai kotak -->
            <div class="col-md-4">
                <div class="thumbnail">
                    <img class="image" src="<?php echo $product['gambar']?>" alt="...">
                    <div class="caption">
                        <h3><?php echo $product['nama']?></h3>
                        <p><?php echo $product['harga']?></p>
                        <p>Deskripsi :<br/><?php echo $product['deskripsi']?></p>
                        <p><a href="#" class="btn btn-primary" role="button">Beli</a> </p>
                    </div>
                </div>
            </div>

            <?php
        }
        ?>

        
        
    </div>

</div>

</body>
</html>