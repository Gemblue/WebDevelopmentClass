<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Data Aktor Film</title>
<link  href="assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/css/style.css">
<link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">

</head>
<body>

<div class="container">
    <div class="header">
        <h2>DAFTAR 10 KOMIKA YANG MENJADI AKTOR FILM</h2>
    </div>
    <table class="table table-striped">
        <thead>
            <tr>                
                <th>Photo</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Tgl Lahir</th>
                <th>Link Profile</th>
                <th>Film</th>
            </tr>
        </thead>
        <tbody>
        <?php
        $content = file_get_contents('artists.json');
        $artists = json_decode($content, true);

        foreach ($artists as $artist) 
        {
            ?>
            <tr>                    
                <td><img  width="100" src="<?php echo $artist['photo'] ?>"></td>
                <td><?php echo $artist['nama'] ?></td>
                <td><?php echo $artist['umur'] ?></td>
                <td><?php echo $artist['tgl_lahir'] ?></td>
                <td><a class="profil profillink" href="<?php echo $artist['profile'] ?>" target="blank">Profile</a></td>
                <td><?php echo $artist['film'] ?></td>
            </tr>
            <?php
        }
        ?>
            
            
        </tbody>
        </table>
    </div>
    </body>
    </html>