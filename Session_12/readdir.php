<?php
$dir = "./content";

if (is_dir($dir))
{
    if ($dh = opendir($dir))
    {
        while (($file = readdir($dh)) !== false)
        {
            if ($file != '.' && $file != '..')
            {
                echo $file . "<br>";
            }
        }

        closedir($dh);
    }
}